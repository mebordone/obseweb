# -*- coding: utf-8 -*-

from math import *
from pprint import pprint
import util
from Parser import Parser
from cluster import *

from django.core.management import setup_environ
from Obseweb import settings
setup_environ(settings)
from Obseweb.documents.models import Articulo

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class VectorSpace:
	""" A algebraic model for representing text documents as vectors of identifiers. 
	A document is represented as a vector. Each dimension of the vector corresponds to a 
	separate term. If a term occurs in the document, then the value in the vector is non-zero.
	"""
	#Documents
	#Number of documents
	documentSize = 0
	#documents
	documents  = []
	#Mapping of vector index to keyword
	vectorKeywordIndex=[]
	#Word ocurrence on documents
	ocurrenceMatrix=[]
	#wrd/document ti.idf 
	tfidfMatrix=[]
	#document similarity
	documentSimilarity = []
	#clusters
	clusters = []
	#Tidies terms
	parser=None


	def __init__(self, documents=[]):
		self.documentVectors=[]
		self.parser = Parser()
		if(len(documents)>0):
			self.build(documents)

	def __getTermDocumentOccurences(self,col):
		""" Find how many documents a term occurs in"""

		termDocumentOccurences=0
		
		cols = len(self.vectorKeywordIndex)
		rows = self.documentSize

		for n in xrange(0,rows):
			if self.ocurrenceMatrix[n][col]>0: #Term appears in document
				termDocumentOccurences+=1 
		return termDocumentOccurences
	
	def getDocumentSimilarity (self):
		"""Get a document similarity matrix"""
		col = self.documentSize
		row = self.documentSize
		documentSimilarity=[[0 for x in range(col)] for y in range(row)]
		i=0
		j=0
		for i in range(self.documentSize):
			for j in range(i,self.documentSize):
				documentSimilarity [i][j] =  util.cosine(self.documentVectors[i], self.documentVectors[j])
				documentSimilarity [j][i] = documentSimilarity [i][j]
		return documentSimilarity
		
	def build(self,documents):
		""" Create the vector space for the passed document strings """
		self.documentSize=len(documents)
		self.documents = documents
		self.vectorKeywordIndex = self.getVectorKeywordIndex(documents)
		self.ocurrenceMatrix = self.getOcurrenceMatrix(documents)
		self.tfidfMatrix = self.getTfIdfMatrix()
		self.makeDocumentVectors(documents)
			
	def getVectorKeywordIndex(self, documentList):
		""" create the keyword associated to the position of the elements within the document vectors """
		vocabularyList = []
		#for document add the pos-tag words
		for document in documentList:
			text = document.titulo_tags + u'\n' + document.bajada_tags + u'\n' + document.texto_tags
			text = text.split('\n')
			for word in text:
				word = word.split('\t')
				if len(word) >=2:
					if word[2].count(u'unknown') > 0:
						vocabularyList.append(word[0])
					else: 
						vocabularyList.append(word[2])
		#Remove common words which have no search value
		vocabularyList = util.removeStopWords(vocabularyList)
		uniqueVocabularyList = util.removeDuplicates(vocabularyList)

		vectorIndex={}
		offset=0
		#Associate a position with the keywords which maps to the dimension on the vector used to represent this word
		for word in uniqueVocabularyList:
			vectorIndex[word]=offset
			offset+=1
		return vectorIndex  #(keyword:position)

	def getOcurrenceMatrix(self,documentList):
		""" Create the matrix with the ocurrence number of a word on a document """
		col = len(self.vectorKeywordIndex)
		row = len(documentList)
		oMatrix=[[0 for x in range(col)] for y in range(row)]
		for i in range(row):
			document = documentList[i]
			vocabularyList = []
			text = document.titulo_tags + u'\n' + document.bajada_tags
			text = text.split('\n')
			for word in text:
				word = word.split('\t')
				if len(word) >=2:
					if word[2].count(u'unknown') > 0:
						vocabularyList.append(word[0])
					else: 
						vocabularyList.append(word[2])
			#Remove common words which have no search value
			vocabularyList = util.removeStopWords(vocabularyList)
			for word in vocabularyList:
				oMatrix[i][self.vectorKeywordIndex[word]] += 2
			text = document.texto_tags
			text = text.split('\n')
			for word in text:
				word = word.split('\t')
				if len(word) >=2:
					if word[2].count(u'unknown') > 0:
						vocabularyList.append(word[0])
					else: 
						vocabularyList.append(word[2])
			#Remove common words which have no search value
			vocabularyList = util.removeStopWords(vocabularyList)
			for word in vocabularyList:
				oMatrix[i][self.vectorKeywordIndex[word]] += 1
		return oMatrix

	def getTfIdfMatrix(self):	
		""" Apply TermFrequency(tf)*inverseDocumentFrequency(idf) for each ocurrenceMatrix element. 
			This evaluates how important a word is to a document in a corpus
	   		With a document-term matrix: matrix[x][y]
			tf[x][y] = frequency of term y in document x / frequency of all terms in document x
			idf[x][y] = log( abs(total number of documents in corpus) / abs(number of documents with term y)  )
			Note: This is not the only way to calculate tf*idf
		"""
		documentTotal = self.documentSize
		rows = self.documentSize
		cols = len(self.vectorKeywordIndex)
		matrix = [[0 for x in range(cols)] for y in range(rows)]
		for row in xrange(0, rows): #For each document
			wordTotal= reduce(lambda x, y: x+y, self.ocurrenceMatrix[row] )
			for col in xrange(0,cols): #For each term
 				#For consistency ensure all self.matrix values are floats
				matrix[row][col] = float(self.ocurrenceMatrix[row][col])
				if matrix[row][col]!=0:
					termDocumentOccurences = self.__getTermDocumentOccurences(col)
					termFrequency = log(float(wordTotal)/matrix[row][col])
					inverseDocumentFrequency = log(abs(documentTotal / float(termDocumentOccurences)))
					matrix[row][col]=termFrequency*inverseDocumentFrequency
		return matrix
        
	def makeDocumentVectors(self,documents):
		for i in range(len(self.tfidfMatrix)):
			documents[i].setDocumentVector(list(self.tfidfMatrix[i]))
    
	def makeVector(self, wordString):
		""" @pre: unique(vectorIndex) """
		wordString = wordString.lower()
		#Initialise vector with 0's
		vector = [0] * len(self.vectorKeywordIndex)
		documentTotal = self.documentSize
		wordList = self.parser.tokenise(wordString)
		wordList = self.parser.removeStopWords(wordList)
		wordList = [word for word in wordList if word in self.vectorKeywordIndex ]
		wordTotal= len(wordList)
		for word in wordList:
				vector[self.vectorKeywordIndex[word]] += 1
		for term in range(len(vector)): #For each term
			#For consistency ensure all self.matrix values are floats
			vector[term] = float(vector[term])
			if vector[term]!=0:
				termDocumentOccurences = self.__getTermDocumentOccurences(term)
				termFrequency = vector[term] / float(wordTotal)
				inverseDocumentFrequency = log(abs(documentTotal / float(termDocumentOccurences)))
				vector[term]=termFrequency*inverseDocumentFrequency
		return vector

	def buildQueryVector(self, termList):
		""" convert query string into a term vector """
		query = self.makeVector(" ".join(termList))
		return query

	def related(self,documentId):
		""" find documents that are related to the document indexed by passed Id within the document Vectors"""
		ratings = []
		for i in range (len(self.documents)):
			elem =(util.cosine(self.documents[documentId].getDocumentVector(),\
								self.documents[i].getDocumentVector()),i)
			ratings.append(elem)
		ratings.sort(reverse=True)
		return ratings

	def search(self,searchList):
		""" search for documents that match based on a list of terms """
		queryVector = self.buildQueryVector(searchList)
		ratings = []
		for i in range (len(self.documents)):
			elem =(util.cosine(queryVector,	self.documents[i].getDocumentVector()),i)
			ratings.append(elem)
		ratings.sort(reverse=True)
		return ratings
	def getClusters (self):
		""" Make the magic clustering """
		
		def sim (doc1,doc2):
			return util.cosine(doc1.getDocumentVector(),doc1.getDocumentVector())	
		cl = HierarchicalClustering(documents,sim, linkage='average')
		cl.cluster()
		self.clusters = cl


#~ documents = ["The cat in the hat disabled", "A cat is a fine pet ponies.", "Dogs and cats make good pets.","I haven't got a hat.","One Ring Ring to Rule them all"]
Articles = Articulo.objects.all()
documents = Articles[:10]
for document in documents:
	print document.titulo
vectorSpace = VectorSpace(documents)
#~ pprint(vectorSpace.tdfidfMatrix)
#~ matrix = vectorSpace.getDocumentSimilarity()
#~ out = open('out2.txt','w')

#~ for i in range(len(matrix)):
	#~ for j in range(len(matrix)):
		#~ if i<j:
			#~ out.write('%d\t%d\t%f\n' % (i,j,matrix[i][j])) 
		#~ else:
			#~ out.write('%d\t%d\t%f\n' % (i,j,0))
 
 
pprint(vectorSpace.related(2))
pprint(vectorSpace.search(["museo"]))
#vectorSpace.getClusters()
#pprint (vectorSpace.clusters.getlevel(0.99999999999999934))
#~ pprint (vectorSpace.vectorKeywordIndex)
#~ pprint (vectorSpace.ocurrenceMatrix)
#~ pprint (vectorSpace.tfidfMatrix)
