from django.core.management import setup_environ
from Obseweb import settings
setup_environ(settings)
from Obseweb.documents.models import Articulo
from treetagger import treetaggerwrapper


from medios import lavoz
Articles = Articulo.objects.all()
for article in Articles:

    #~ tagger = treetaggerwrapper.TreeTagger(TAGLANG='es',TAGDIR='treetagger')
#~ 
    #~ Titulo_tag = tagger.TagText(article.titulo)
    #~ TituloTagTexto = '\n'.join(Titulo_tag)
        #~ 
    #~ Bajada_tag = tagger.TagText(article.bajada)
    #~ BajadaTagTexto = '\n'.join(Bajada_tag)
#~ 
    #~ Texto_tag = tagger.TagText(article.texto)
    #~ TextoTagTexto = '\n'.join(Texto_tag)

    article.titulo_tags = ""
    article.bajada_tags = ""
    article.texto_tags = ""

    f = open("Lavoz %s.xml" % (article.id),"w")
    f.write( "<medio>%s</medio>" % (lavoz.replaceinhtml(article.medio)))
    f.write( "<fecha>%s</fecha>" % (article.fecha_pub))
    f.write( "<autor>%s</autor>" % (lavoz.replaceinhtml(article.autor)))
    f.write( "<seccion>%s</seccion>" % (lavoz.replaceinhtml(article.seccion)))
    f.write( "<titulo>%s</titulo>" % (lavoz.replaceinhtml(article.titulo)))
    f.write( "<volanta>%s</volanta>" % (lavoz.replaceinhtml(article.volanta)))
    f.write( "<bajada>%s</bajada>" % (lavoz.replaceinhtml(article.bajada)))
    f.write( "<texto>%s</texto>" % (lavoz.replaceinhtml(article.texto)))
    f.close()
