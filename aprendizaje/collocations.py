# Pretende descubrir collocations en un archivos de texto.
# Para esto armamos bigramas y contamos sus frecuencias
# quitando de ellos las palabras funcionales o vacias.

import re

from django.core.management import setup_environ
from Obseweb import settings
setup_environ(settings)
from Obseweb.documents.models import Articulo

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

def EncontrarCollocations():
    filename = 'nota.txt'
    emptywords_file = '/home/mebordone/Programming/PLN/Observatorio/aprendizaje/palabrasvacias1.txt'

    # Palabras que voy a filtrar \s+ --> match any whitespace(s)
    empty = re.split('\s+', file(emptywords_file).read())
    emptyset = set(empty)

    freq_bigrams = {}
    freq_trigrams = {}
    Articles = Articulo.objects.all()

    def count_bigrams(tagged_text):
        coll_list = []
        tagged = tagged_text.split('\n')
        for i in range(0,len(tagged)-1):
            now = tagged[i].split('\t')
            next = tagged[i+1].split('\t')
            if (now[0] not in emptyset and next[0] not in emptyset):
                try:
                    entry = now[0] +' '+ next[0]+ ' (%s|%s)' % (now[1],next[1])
                    coll_list.append(entry)
                except IndexError:
                    i=i+1
        for coll in coll_list:
            try: 
                freq_bigrams[coll] += 1
            except: 
                freq_bigrams[coll] = 1
        return 0

    def count_trigrams(tagged_text):
        coll_list = []
        tagged = tagged_text.split('\n')
        for i in range(0,len(tagged)-2):
            now = tagged[i].split('\t')
            next = tagged[i+1].split('\t')
            nextnext = tagged[i+2].split('\t')
            if (now[0] not in emptyset and next[0] not in emptyset and nextnext[0] not in emptyset):
                try:
                    entry = now[0]+' '+ next[0]+' '+ nextnext[0]+' (%s|%s|%s)' % (now[1],next[1],nextnext[1])
                    coll_list.append(entry) 
                except IndexError:
                    i=i+1
                
        for coll in coll_list:
            try: 
                freq_trigrams[coll] += 1
            except: 
                freq_trigrams[coll] = 1
        return 0

    for article in Articles:
        count_bigrams(article.titulo_tags)
        count_trigrams(article.titulo_tags)
        count_bigrams(article.bajada_tags)
        count_trigrams(article.bajada_tags)
        count_bigrams(article.texto_tags)
        count_trigrams(article.texto_tags)

    outpb = open('bigramas.txt','w')
    freq_list = freq_bigrams.items()
    listfreq = []
    for coll,freq in freq_list:
        listfreq.append((freq,coll))
    listfreq.sort()
    listfreq.reverse()
    for freq,coll in listfreq:
        big = "%s %s\n" % (freq,coll)
        outpb.write(big)
    outpb.close()

    outpt = open('trigramas.txt','w') 
    freq_list = freq_trigrams.items()
    listfreq = []
    for coll,freq in freq_list:
        listfreq.append((freq,coll))
    listfreq.sort()
    listfreq.reverse()
    for freq,coll in listfreq:
        outpt.write("%s %s\n" % (freq,coll))
    outpt.close()
    return 0

def EsEntidad(string,cap,nocap)
    for token in string
        
    
