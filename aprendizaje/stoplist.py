# -*- coding: utf-8 -*-
import re

from django.core.management import setup_environ
from .. import Obseweb
setup_environ(settings)
from ..Obseweb.documents.models import Articulo
from nltk import FreqDist

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

def tag2w (tagged):
	words = []
	tagged = tagged.split('\n')
	for i in range(0,len(tagged)-1):
		words.append(tagged[i].split('\t')[0])
	return words

Articles = Articulo.objects.all()
text = []
article = Articles[0]
#for article in Articles:
print article.titulo
print article.bajada
print article.texto
text = text + tag2w(article.titulo_tags) + tag2w(article.bajada_tags) + tag2w(article.texto_tags)

fdist = FreqDist(text)
print fdist
