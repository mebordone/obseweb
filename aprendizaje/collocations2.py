# Pretende descubrir collocations en un archivos de texto.
# Para esto armamos bigramas y contamos sus frecuencias
# quitando de ellos las palabras funcionales o vacias.

import re

from django.core.management import setup_environ
from Obseweb import settings
setup_environ(settings)
from Obseweb.documents.models import Articulo

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

emptywords_file = '/home/mebordone/Programming/PLN/Observatorio/aprendizaje/palabrasvacias1.txt'
# Palabras que voy a filtrar \s+ --> match any whitespace(s)
empty = re.split('\s+', file(emptywords_file).read())
emptyset = set(empty)

def count_ngramas(n,freq_ngramas,tagged_text):
    coll_list = []
    tagged = tagged_text.split('\n')
    for i in range(0,len(tagged)-(n-1)):
        entryw =''
        entrypos=' ('
        count=True
        for j in range(0,n):
            token = tagged[i+j].split('\t')
            if (token[0] not in emptyset):
                try:
                    add = ' '
                    add = add + token[0]
                    entryw = entryw + add
                    print entryw
                    entrypos = entrypos +'%s|' % (token[1])
                except IndexError:
                    i=i+1
                    count = False
            else:
                count = False
        if (count):coll_list.append(entryw+entrypos)
    for coll in coll_list:
        try: 
            freq_ngramas[coll] += 1
        except: 
            freq_ngramas[coll] = 1
    return freq_ngramas

def Encontrarngramas(n):
    freq_ngramas = {}
    Articles = Articulo.objects.all()

    for article in Articles:
        text = article.titulo_tags + article.bajada_tags + article.texto_tags
        freq_ngramas = count_ngramas(n,freq_ngramas,article.titulo_tags)

    outpb = open('%d-gramas.txt'%(n),'w')
    freq_list = freq_ngramas.items()
    listfreq = []
    for coll,freq in freq_list:
        listfreq.append((freq,coll))
    listfreq.sort()
    listfreq.reverse()
    for freq,coll in listfreq:
        big = "%s %s\n" % (freq,coll)
        outpb.write(big)
    outpb.close()

    return 0

#~ def EsEntidad(string,cap,nocap):
    #~ for token in string:
        
    
