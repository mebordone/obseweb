# CorpusDump

En esta carpeta puede encontrar corpus de texto de medios en formato texto plano.

El Corpus de LaVoz fué generado en el 2010 por Matías Bordone mediante scrapping de la página De "Lla voz del interior" periódico de la ciudad de Córdoba, Argentina, por lo que corresponde a español rioplatense." para el proyecto Obseweb. Encontratá el título y el contenido de la noticia separado por "-" (sin las comillas)

Los otros dos corpus fueron tomados de ejemplo de la materia de TextMining famaf.
