#!/usr/bin/python
# -*- coding: utf8 -*-
import re
import urllib2
from BeautifulSoup import UnicodeDammit

def decode_html(html_string):
        converted = UnicodeDammit(html_string, isHTML=True)
        if not converted.unicode:
                raise UnicodeDecodeError(
                        "Failed to detect encoding, tried [%s]",
                        ', '.join(converted.triedEncodings))
        return converted.unicode
        
url='http://www.lavoz.com.ar/ciudadanos/clausuran-planta-transformadora-de-epec-en-san-antonio'
doc  = urllib2.urlopen(url)
doc2 = decode_html(doc.read())

## Quiero agarrar este pedazo de texto ##
## class="FFUsuario clearfix"><ul><li><span>29/12/2010 09:14</span> , por <a
## href="/user/58372" title="Redacción LAVOZ">Redacción LAVOZ</a></li></ul><div

authorPattern = re.compile(r'class="FFUsuario clearfix"><ul><li>(a)</li>')
print authorPattern.search(doc2).groups()


