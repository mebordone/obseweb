from math import *
from pprint import pprint
from Parser import Parser
import util

class VectorSpace:
	""" A algebraic model for representing text documents as vectors of identifiers. 
	A document is represented as a vector. Each dimension of the vector corresponds to a 
	separate term. If a term occurs in the document, then the value in the vector is non-zero.
	"""
	
	documentSize = 0
	#Collection of document term vectors
	documentVectors = []

	#Mapping of vector index to keyword
	vectorKeywordIndex=[]
	ocurrenceMatrix=[]
	tfidfMatrix=[]
	#Tidies terms
	parser=None


	def __init__(self, documents=[]):
		self.documentVectors=[]
		self.parser = Parser()
		if(len(documents)>0):
			self.build(documents)

	def __getTermDocumentOccurences(self,col):
		""" Find how many documents a term occurs in"""

		termDocumentOccurences=0
		
		cols = len(self.vectorKeywordIndex)
		rows = self.documentSize

		for n in xrange(0,rows):
			if self.ocurrenceMatrix[n][col]>0: #Term appears in document
				termDocumentOccurences+=1 
		return termDocumentOccurences


	def build(self,documents):
		""" Create the vector space for the passed document strings """
		self.documentSize=len(documents)
		self.vectorKeywordIndex = self.getVectorKeywordIndex(documents)
		self.ocurrenceMatrix = self.getOcurrenceMatrix(documents)
		self.tdfidfMatrix = self.getTfIdfMatrix()
		self.documentVectors = [self.makeVector(document) for document in documents]

	def getVectorKeywordIndex(self, documentList):
		""" create the keyword associated to the position of the elements within the document vectors """

		#Mapped documents into a single word string	
		vocabularyString = " ".join(documentList)
			
		vocabularyList = self.parser.tokenise(vocabularyString)
		#Remove common words which have no search value
		vocabularyList = self.parser.removeStopWords(vocabularyList)
		uniqueVocabularyList = util.removeDuplicates(vocabularyList)
		
		vectorIndex={}
		offset=0
		#Associate a position with the keywords which maps to the dimension on the vector used to represent this word
		for word in uniqueVocabularyList:
			vectorIndex[word]=offset
			offset+=1
		return vectorIndex  #(keyword:position)

	def getOcurrenceMatrix(self,documentList):
		col = len(self.vectorKeywordIndex)
		row = len(documentList)
		oMatrix=[[0 for x in range(col)] for y in range(row)]
		for i in range(row):
			document = documentList[i]
			vocabularyList = self.parser.tokenise(document)
			#Remove common words which have no search value
			vocabularyList = self.parser.removeStopWords(vocabularyList)
			for word in vocabularyList:
				oMatrix[i][self.vectorKeywordIndex[word]] += 1
		return oMatrix

	def getTfIdfMatrix(self,):	
		""" Apply TermFrequency(tf)*inverseDocumentFrequency(idf) for each matrix element. 
		    This evaluates how important a word is to a document in a corpus
	   	    With a document-term matrix: matrix[x][y]
			tf[x][y] = frequency of term y in document x / frequency of all terms in document x
			idf[x][y] = log( abs(total number of documents in corpus) / abs(number of documents with term y)  )
		    Note: This is not the only way to calculate tf*idf
		"""
		documentTotal = self.documentSize
		rows = self.documentSize
		cols = len(self.vectorKeywordIndex)
		matrix = [[0 for x in range(cols)] for y in range(rows)]
		for row in xrange(0, rows): #For each document
			wordTotal= reduce(lambda x, y: x+y, self.ocurrenceMatrix[row] )
			for col in xrange(0,cols): #For each term
 				#For consistency ensure all self.matrix values are floats
				matrix[row][col] = float(self.ocurrenceMatrix[row][col])
				if matrix[row][col]!=0:
					termDocumentOccurences = self.__getTermDocumentOccurences(col)
					termFrequency = log(matrix[row][col] / float(wordTotal))
					inverseDocumentFrequency = log(abs(documentTotal / float(termDocumentOccurences)))
					matrix[row][col]=termFrequency*inverseDocumentFrequency
		return matrix

	def makeVector(self, wordString):
		""" @pre: unique(vectorIndex) """

		#Initialise vector with 0's
		vector = [0] * len(self.vectorKeywordIndex)
		documentTotal = self.documentSize
		wordList = self.parser.tokenise(wordString)
		wordList = self.parser.removeStopWords(wordList)
		wordList = [word for word in wordList if word in self.vectorKeywordIndex ]
		wordTotal= len(wordList)
		for word in wordList:
				vector[self.vectorKeywordIndex[word]] += 1
		for term in range(len(vector)): #For each term
			#For consistency ensure all self.matrix values are floats
			vector[term] = float(vector[term])
			if vector[term]!=0:
				termDocumentOccurences = self.__getTermDocumentOccurences(term)
				termFrequency = vector[term] / float(wordTotal)
				inverseDocumentFrequency = log(abs(documentTotal / float(termDocumentOccurences)))
				vector[term]=termFrequency*inverseDocumentFrequency
		return vector


	def buildQueryVector(self, termList):
		""" convert query string into a term vector """
		query = self.makeVector(" ".join(termList))
		return query


	def related(self,documentId):
		""" find documents that are related to the document indexed by passed Id within the document Vectors"""
		ratings = []
		for i in range (0,len(self.documentVectors)):
			ratings.append((util.cosine(self.documentVectors[documentId], self.documentVectors[i]),i))
		ratings.sort(reverse=True)
		return ratings


	def search(self,searchList):
		""" search for documents that match based on a list of terms """
		queryVector = self.buildQueryVector(searchList)
		ratings = []
		for i in range (0,len(self.documentVectors)):
			ratings.append((util.cosine(queryVector, self.documentVectors[i]),i))
		ratings.sort(reverse=True)
		return ratings


if __name__ == '__main__':
	#test data
	documents = ["The cat in the hat disabled", "A cat is a fine pet ponies.", "Dogs and cats make good pets.","I haven't got a hat.","One Ring Ring to Rule them all"]

	vectorSpace = VectorSpace(documents)
	pprint(vectorSpace.related(2))
	pprint(vectorSpace.search(["hat"]))
	pprint(vectorSpace.vectorKeywordIndex)
	pprint(vectorSpace.ocurrenceMatrix)

