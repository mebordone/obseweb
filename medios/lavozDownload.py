#!/usr/bin/python
# -*- coding: utf8 -*-

import re
import urllib2
import datetime
import commands
import doken

def getDaysLinks(inicio,fin):
    """ Toma una fecha de inicio y otra de fin en formato date
    Devuleve una lista de tuplas (fecha articulo,link articulo)
    entre la fecha inicio y fin"""
    limite = datetime.date(2010,04,15) #Fecha de cambio del cms de la voz
    fecha = inicio
    if fecha < limite : fecha = limite     # empieza antes del limite -> empiece en el limite
    links = []
    while fecha <= fin:
        dia = fecha.day
        mes = fecha.month
        ano = fecha.year
        link = "http://www.lavoz.com.ar/edicion_impresa?fecha=%02d/%02d/%d" % (dia,mes,ano)
        links = links + [link]
        fecha = fecha + datetime.timedelta(days=1)
    return links


articlesAreaPattern = re.compile(r'class="Impresa_right">(.*?)class="Columnas clearfix"><div',re.DOTALL)
articlesPattern = re.compile(r'<h3 class="Titulo"><a\n?href="(.*?)" title=".*?</a></h3><p')
def getArticleLinks (url):
    print url
    filepage = urllib2.urlopen(url)
    page = filepage.read()
    articleArea = articlesAreaPattern.search(page).groups()[0]
    rlinks = articlesPattern.findall(articleArea)
    links =[]
    for rlink in rlinks:
        links = links + ["http://www.lavoz.com.ar%s" % (rlink)]
    return links


fecha = datetime.date(2010,04,15)

daylink = getDaysLinks(fecha,fecha)
for link in daylink:
    articlelinks = getArticleLinks(link)
    for article in articlelinks:
        print article
        executed = commands.getoutput('wget -P%s %s' % (doken.DOCPATH,article))
