#!/usr/bin/python
# -*- coding: utf8 -*-

DOCPATH = './Documentos'

ID = "Identification"
MEDIA = "Source Media"
AUTHORS = "Authors"
DATELINE = "Dateline"
PUBDATE = "Publication Date"
ACTDATE = "Actualization Date"
LEADP = "Lead Parragraph"
HEAD = "Title"
BODY = "Article Itself"
SOURCE = "Source Code"
SECTION = "Section"
VOLANTA = "Volanta"
TAGS = "Tags"
LINK = "Link"
RELATED = "Related Link"
NCOMMENTS = "Commenst Number"
NPICS = "Pics Number"
NVIDS = "Vids Number"


class Doken:
    """ Document unity """
    d = {}

class Library:
    """ Documentos """
    docs = {}


