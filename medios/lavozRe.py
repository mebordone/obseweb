#!/usr/bin/python
# -*- coding: utf8 -*-
import re
import os
import sys
import nltk
from doken import *
import datetime
import cPickle

reload (sys)
sys.setdefaultencoding('utf-8')
sys.path.append('/media/MATUS/PLN/Observatorio')

authorPattern1 = re.compile(r'class="FFUsuario clearfix".*? \| (.*?)</li>')
authorPattern2 = re.compile(r'class="FFUsuario clearfix".*\n.*?>(.*)</a></li>')
datePattern = re.compile(r'class="FFUsuario clearfix"><ul><li><span>(.*?)/(.*?)/(.*?) (.*?):(.*?)</span>')
actdatePattern = re.compile(r'class="Fecha">.*? (\d*?) de (.*?) de (\d*?). <strong>Actualizado <span>(\d*?):(\d*?)</span></strong></p><div')
titlePattern = re.compile(r'class="Art"><h1>(.*?)</h1><h2>(.*?)</h2>')
bodyPattern = re.compile(r'class="Texto clearfix">(.*?)<div\nclass="Compartir"><div',re.DOTALL)
sectionPattern = re.compile(r'class="seccion"><dl><dt><a\n.*?>(.*?)</a')
linkPattern = re.compile(r'rel="canonical" href="(.*?)" /><meta')
volantaPattern = re.compile(r'class="seccion"><dl><dt>.*?</dt>(.*?)</dl>',re.DOTALL)
tagsPattern = re.compile(r'name="keywords" content="(.*?)"><meta')
tags2Pattern = re.compile(r'<a.*?taxonomy.*?>(.*?)\|? ?</a>',re.DOTALL)
commentsPattern = re.compile(r'href="#Comentarios" title="Comentarios" rel="nofollow">(.*?)</a></li><li><span')
picsPattern = re.compile(r'rel="nofollow">Fotos <span>\((\d*?)\)')


monthdic = {'enero':1,
        'febrero':2,
        'marzo':3,
        'abrl:':4,
        'mayo':5,
        'junio:':6,
        'julio':7,
        'agosto':8,
        'septiembre':9,
        'octubre':10,
        'noviembre':11,
        'diciembre':12}

def craw(path = DOCPATH):
    library = {}
    for url in os.listdir(path):
        print url
        ob    = open(path + '/' + url,'r')
        doc = ob.read()
        ob.close()
        d ={}
        d[SOURCE] = doc
        d[MEDIA] = "La Voz del Interior"
        if authorPattern1.search(doc) != None:
                authors = authorPattern1.search(doc).groups()[0]
        elif authorPattern2.search(doc) != None:
                authors = authorPattern2.search(doc).groups()[0]
        splitted = authors.split(', ')
        d[AUTHORS] = []
        for word in splitted:
            d[AUTHORS] = d[AUTHORS] + word.split(' y ')
        year = int(datePattern.search(doc).groups()[2])
        month = int(datePattern.search(doc).groups()[1])
        day = int(datePattern.search(doc).groups()[0])
        hour = int(datePattern.search(doc).groups()[3])
        minute = int(datePattern.search(doc).groups()[4])
        d[PUBDATE] = datetime.datetime(year,month,day,hour,minute)
        year = int(actdatePattern.search(doc).groups()[2])
        month = monthdic[actdatePattern.search(doc).groups()[1]]
        day = int(actdatePattern.search(doc).groups()[0])
        hour = int(actdatePattern.search(doc).groups()[3])
        minute = int(actdatePattern.search(doc).groups()[4])
        d[ACTDATE] = datetime.datetime(year,month,day,hour,minute)
        d[HEAD] = nltk.clean_html(titlePattern.search(doc).groups()[0])
        d[LEADP] = nltk.clean_html(titlePattern.search(doc).groups()[1])
        body = bodyPattern.search(doc).groups()[0]
        d[BODY] = nltk.clean_html(body)
        d[SECTION] = nltk.clean_html(sectionPattern.search(doc).groups()[0])
        d[VOLANTA] = nltk.clean_html(volantaPattern.search(doc).groups()[0])
        d[LINK] = nltk.clean_html(linkPattern.search(doc).groups()[0])
        d[TAGS] = nltk.clean_html(tagsPattern.search(doc).groups()[0]).split(', ')[:-1]
        tags2 = tags2Pattern.findall(body)
        if tags2: d[TAGS] = d[TAGS] + tags2
        d[NCOMMENTS] = int(nltk.clean_html(commentsPattern.search(doc).groups()[0]))
        pics = picsPattern.search(doc)
        if pics: d[NPICS] = int(pics.groups()[0])
        d[ID] = d[PUBDATE].isoformat()+d[HEAD]
        library[d[ID]] = d
    return library

def save (d):
    bckp = open ('library.bcp','w')
    cPickle.dump(d,bckp)
    bckp.close()

def get ():
    bckp = open ('library.bcp','r')
    d = cPickle.load(bckp)
    bckp.close()
    return d

library = craw()
for d in library:
    print d
save(library)
