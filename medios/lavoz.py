# -*- coding: utf-8 -*-
# Modulo Lavoz.com.ar 15/04/2010 - Hoy

### Declaraciones para el uso de los articulos ###
from django.core.management import setup_environ
from Obseweb import settings
setup_environ(settings)
from Obseweb.documents.models import Articulo
from treetagger import treetaggerwrapper

from lxml import html, etree
import nltk
import datetime
import re
import urllib2
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

def uniq(list): 
  output = [] 
  for x in list: 
    if x not in output: 
      output.append(x) 
  return output 

def bajarLaVoz(desde,hasta):
    """ Descarga todos los articlos de la voz entre las fechas
    'desdese' y 'hasta' """
    links = links_articulos(desde,hasta)
    print "obtenidos links"
    for link in links:
        check=0
        while check == 0:
            #~ guardar_articulo (link[1],link[0])
            #~ check = 1
            try: 
                guardar_articulo (link[1],link[0])
                check = 1
            except IndexError:
                print "Unexpected error:", sys.exc_info()[0]
                check = 1
            except:
                print "Unexpected error:", sys.exc_info()[0]
                check = 0
    return 0

#Funciones de Busqueda de Articulos
def obtener_links_notas(archivo,fecha):
    """Toma la url de las notas de un dia y la fecha en formato date
    Devuelve una lista con los pares (fecha_articulo,link_articulo,) """
    print archivo
    pagina = html.parse(archivo)
    todos_links = pagina.xpath('/html/body/div[@class="Portadilla layout"]/div[@class="Contenido Impresa"]/div[@class="Columnas clearfix"]/div[@class="CD clearfix"]/div[@class="Impresa_right"]/*/*/div[@class="Inner clearfix"]/div[@class="Container"]/h3//@href')
    links_notas = []
    for link in todos_links:
        if re.search("/node/",link):
            link_nota = "http://www.lavoz.com.ar%s" % (link)
            links_notas.append ((fecha,link_nota))
    return uniq(links_notas)
    
def links_articulos(inicio,fin):
    """ Toma una fecha de inicio y otra de fin en formato date
    Devuleve una lista de tuplas (fecha articulo,link articulo) 
    entre la fecha inicio y fin"""
    limite = datetime.date(2010,04,15) #Fecha de cambio del cms de la voz
    fecha = inicio
    if fecha < limite : fecha = limite     # empieza antes del limite -> empiece en el limite
    links = []
    while fecha <= fin :
        dia = fecha.day
        mes = fecha.month
        ano = fecha.year
        archivo = "http://www.lavoz.com.ar/edicion_impresa?fecha=%02d/%02d/%d" % (dia,mes,ano)
        check = 0
        while check == 0:
            try:
                links_dia = obtener_links_notas(archivo, fecha)
                check = 1
            except:
                check=0
        links = links + links_dia
        fecha = fecha + datetime.timedelta(days=1)
    return uniq(links)

# Ver: http://codespeak.net/lxml/elementsoup.html
# Titulo: Using only the encoding detection

from BeautifulSoup import UnicodeDammit

def decode_html(html_string):
        converted = UnicodeDammit(html_string, isHTML=True)
        if not converted.unicode:
                raise UnicodeDecodeError(
                        "Failed to detect encoding, tried [%s]",
                        ', '.join(converted.triedEncodings))
        return converted.unicode
        
def replaceinhtml (texto):
    """ Reemplaza cadenas extrañas metidas por lxml por sus letras
    equivalentes """
    texto = texto.replace(u'&#195;&#179;',u'ó')
    texto = texto.replace(u'&#195;&#173;',u'í')
    texto = texto.replace(u'&#195;&#161;',u'á')
    texto = texto.replace(u'&#195;&#186;',u'ú')
    texto = texto.replace(u'&#195;&#177;',u'ñ')
    texto = texto.replace(u'&#195;&#169;',u'é')
    texto = texto.replace(u'&#226;&#8364;&#339;',u'"')
    texto = texto.replace(u'&#226;&#8364;?',u'"')
    texto = texto.replace(u'&#194;&#161;',u'¡')
    texto = texto.replace(u'&#194;&#176;',u'º')
    texto = texto.replace(u'&#194;&#191;',u'¿')
    texto = texto.replace(u'&#195;&#353;',u'Ú') # Buscar para la otras mayusculas.
    texto = texto.replace(u'&#195;&#8240;',u'É')    
    texto = texto.replace(u'&#226;&#8364;&#168;',u'')
    texto = texto.replace(u'&#226;&#8364;&#169;',u'')
    texto = texto.replace(u'&#226;&#8364;&#8220;',u'-')
    texto = texto.replace(u'&#194;&#160;',u' ')
    texto = texto.replace(u'&#194;&#186;',u'º')
    texto = texto.replace(u'&#195; ',u'Á')
    texto = texto.replace(u'&#226;&#8364;&#166;',u'...')
    texto = texto.replace(u'&#195;&#171;',u'ë')
    texto = texto.replace(u'&#194;&#170;',u'ª')
    texto = texto.replace(u'&#194;&#8217;',u'')
    texto = texto.replace(u'&#226;&#8364;&#8218;',u' ')
    texto = texto.replace(u'&#195;?',u'Í')
    return texto

def guardar_articulo (url,fecha):
    """ Toma la url de una nota y la fecha
    Y devuelve un xml del atriculo de llamado 'nombre' """
    print "guardo articulo"
    doc = urllib2.urlopen(url)
    Notaxml = html.fromstring(decode_html(doc.read()))
    Medio = "La Voz del Interior"
    print Medio
    #~ Autorxml= Notaxml.xpath("/html/body/div[@class='layout']/\
    #~ div[@class='Contenido']/div[@class='Columnas clearfix']/*/\
    #~ div[@class='CD clearfix']/*/div[@class='Art']/\
    #~ div[@class='FFUsuario clearfix']/ul/li")
    #~ Autorstring = etree.tostring(Autorxml[0])
    #~ Autorstring = nltk.clean_html(Autorstring)
    #~ Autorstring = replaceinhtml(Autorstring)
    #~ if Autorstring.count('|') > 0:
        #~ Autorstring = Autorstring.split('| ')[1]
    #~ print Autorstring
    # Se debe hacer una consulta para conseguir la seccion, pero se ve
    # que hay un javascript que usa hover y cambia el html cuando lo
    # consulto por consola a cuando lo veo online. 
    #~ Seccionxml = Notaxml.xpath("/html/body/div[@class='layout']/\
    #~ div[@class='Articulo']/div/div[@class='Hd Ancho']/div[@class='Ancho']/div[@class='Bottom']/div[@class='barra']/div[@class='Ancho']/ul")
    #~ /li[@class='sf-breadcrumb sfHover']/ul/li[@class='seccion']/dl/dt/a")
    Seccionstring = ''
    print Seccionstring
    Tituloxml = Notaxml.xpath("/html/body/div[@class='layout']/\
    div[@class='Contenido']/div[@class='Columnas clearfix']/*/\
    div[@class='CD clearfix']/*/div[@class='Art']/h2")
    Titulostring = etree.tostring(Tituloxml[0])
    Titulostring = nltk.clean_html(Titulostring)
    Titulostring = replaceinhtml(Titulostring)
    Titulostring = Titulostring + '.'
    print Titulostring
    Volantastring = ''
    Bajadaxml = Notaxml.xpath("/html/body/div[@class='layout']/\
    div[@class='Contenido']/div[@class='Columnas clearfix']/*/\
    div[@class='CD clearfix']/*/div[@class='Art']/p")
    Bajadastring = etree.tostring(Bajadaxml[0])
    Bajadastring = nltk.clean_html(Bajadastring)
    Bajadastring = replaceinhtml(Bajadastring)
    Textoxml = Notaxml.xpath("/html/body/div[@class='layout']/\
    div[@class='Contenido']/div[@class='Columnas clearfix']/*/\
    div[@class='CD clearfix'][2]/div[@class='CM']/*/\
    div[@class='Texto clearfix']")
    Textostring = etree.tostring(Textoxml[0])
    Textostring = Textostring.replace(u'</p>',u'\n')
    Textostring = nltk.clean_html(Textostring)
    Textostring = replaceinhtml(Textostring)
    Fecha_pub = fecha
    Fecha_baj = datetime.date.today()
    Comentarios = int(Notaxml.xpath("/html/body/div[@class='layout']/\
    div[@class='Contenido']/div[@class='Columnas clearfix']/*/\
    div[@class='CD clearfix']/*/div[@class='Art']/\
    div[@class='FFUsuario clearfix']/div[@class='DER Static']/\
    ul[@class='clearfix']/li[@class='Com']/a/text()")[0])
    Num_fotos = 0
    Num_videos = 0

    tagger = treetaggerwrapper.TreeTagger(TAGLANG='es',TAGDIR='treetagger')
    
    Titulo_tag = tagger.TagText(Titulostring)
    TituloTagTexto = '\n'.join(Titulo_tag)
        
    Bajada_tag = tagger.TagText(Bajadastring)
    BajadaTagTexto = '\n'.join(Bajada_tag)

    Texto_tag = tagger.TagText(Textostring)
    TextoTagTexto = '\n'.join(Texto_tag)
    
    articulo = Articulo (
        medio = Medio,
        autor = Autorstring,
        seccion = Seccionstring,
        titulo = Titulostring,
        volanta = Volantastring,
        bajada = Bajadastring,
        texto = Textostring,
        fecha_bajada = Fecha_baj,
        fecha_pub = Fecha_pub,
        #notas_rel = models.ManyToManyField(Articulo, verbose_name="Notas Relacionadas")
        num_comentarios = Comentarios,
        num_fotos = Num_fotos,
        num_videos = Num_videos,
        titulo_tags = TituloTagTexto,
        bajada_tags = BajadaTagTexto,
        texto_tags = TextoTagTexto
    )
    print "%s: %s" % (articulo.fecha_pub, articulo.titulo)
    #~ articulo.save()

    
    print articulo.medio
    print articulo.autor
    print articulo.seccion
    print articulo.volanta
    print articulo.titulo
    print articulo.bajada
    print articulo.texto
    #~ print articulo.fecha_pub
    #~ print articulo.fecha_bajada
    #~ print articulo.num_comentarios
    #~ print articulo.num_fotos
    #~ print articulo.num_videos
    print articulo.titulo_tags
    print articulo.bajada_tags
    print articulo.texto_tags
