import sys

#http://www.scipy.org/
try:
	from numpy import dot
	from numpy.linalg import norm
except:
	print "Error: Requires numpy from http://www.scipy.org/. Have you installed scipy?"
	sys.exit() 

def removeDuplicates(list):
	""" remove duplicates from a list """
	return set(list)


def cosine(vector1, vector2):
	""" related documents j and q are in the concept space by comparing the vectors :
		cosine  = ( V1 * V2 ) / ||V1|| x ||V2|| """
	norm1=norm(vector1)
	norm2=norm(vector1)
	res = 0
	if not((norm1==0) | (norm2==0)):
		res = float(dot(vector1,vector2) / (norm(vector1) * norm(vector2)))
	return res

def removeStopWords(list):
    """ Remove common words which have no search value """
    stopwords = open('aprendizaje/palabrasvacias1.txt', 'r').read().split()
    return [word for word in list if word not in stopwords ]
