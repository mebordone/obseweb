#! /usr/bin/python
# -*- coding: iso-8859-1 -*-

## Run as root ./sample.py
## Reads from stdin and writes to stdout
## For example:
##     ./sample.py <prueba.txt >prueba_out.txt

import libmorfo_python
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

## Modify this line to be your FreeLing installation directory
FREELINGDIR = "/usr";
DATA = FREELINGDIR+"/share/FreeLing/";
LANG="es";

# create options set for maco analyzer. Default values are Ok, except for data files.
op= libmorfo_python.maco_options("es");
op.set_active_modules(1,1,1,1,1,1,1,1,1,0);
op.set_data_files(DATA+LANG+"/locucions.dat", DATA+LANG+"/quantities.dat", DATA+LANG+"/afixos.dat",
                  DATA+LANG+"/probabilitats.dat", DATA+LANG+"/maco.db", DATA+LANG+"/ner/ner.dat",  
                  DATA+"common/punct.dat",DATA+LANG+"/corrector/corrector.dat");

# create analyzers
tk=libmorfo_python.tokenizer(DATA+LANG+"/tokenizer.dat");
sp=libmorfo_python.splitter(DATA+LANG+"/splitter.dat");
mf=libmorfo_python.maco(op);

tg=libmorfo_python.hmm_tagger("es",DATA+LANG+"/tagger.dat",1,2);
sen=libmorfo_python.senses(DATA+LANG+"/senses30.db",0);
nec=libmorfo_python.nec('NP',DATA+LANG+"/nec/nec")

lin = '''El diario El País de Madrid, uno de los cuatro medios del mundo que accedió en forma anticipada a los documentos secretos que filtró WikiLeaks, publica hoy que en esa información se leen "sospechas" de Estados Unidos sobre la presidenta argentina Cristina Fernández de Kirchner.
"Mañana El País ofrecerá detalles, por ejemplo, sobre las sospechas que la presidenta de Argentina, Cristina Fernández de Kirchner, despierta en Washington, hasta el punto de que la Secretaría de Estado llega a solicitar información sobre su estado de salud mental", publica a modo de adelanto El País en la nota titulada La mayor filtración de la historia deja al descubierto los secretos de la política exterior de EE.UU.
El diario español no precisó en qué momento fueron emitidos esos documentos en los que, según dice, se habla sobre la "salud mental" de la Presidenta.
Malvinas. También hay revelaciones respecto del tema de las islas, según explica la nota Aportaciones históricas sobre la liberación de Mandela, la Revolución Islámica o el conflicto de las Malvinas, del diario El País. Para acceder al archivo completo (y en inglés), se puede ingresar a WikiLeaks. 

Diplomacia. Los documentos del Departamento de Estado filtrados revelan un mundo oculto de diplomacia internacional tras bambalinas, con comentarios cándidos de líderes mundiales y detalles de las estrategias estadounidenses.
Los cables diplomáticos secretos divulgados por el sitio de internet WikiLeaks, especializado en filtraciones, y reportados en organizaciones noticiosas de Estados Unidos y Europa proporcionan evaluaciones de líderes mundiales, en ocasiones poco halagadoras, desde los de aliados estadounidenses como Alemania e Italia hasta los de otras naciones como Libia, Irán y Afganistán.
Los cablegramas también contienen revelaciones nuevas sobre las dificultades estadounidenses para lidiar con algunos países con programas nucleares, detallando los temores de Estados Unidos, Israel y el mundo árabe por el programa atómico de Irán, las preocupaciones de Washington en torno al arsenal nuclear de Pakistán, y el análisis en torno a una península coreana unida como una solución a largo plazo a la política hostil de Corea del Norte.
Ninguna de las revelaciones es particularmente escandalosa, pero su publicación podría resultar pro funcionarios involucrados.
En el mismo sentido, pero referido a otro tema, también se filtró el pedido de espionaje de EE.UU hacia funcionarios de la Organización de las Naciona Unidas (UNU), lo que causó varias repercusiones a nivel mundial (ver Documentos secretos revelan que EE.UU. ordenó espiar a la ONU).
Los documentos enviados de antemano por WikiLeaks a los diarios The New York Times; Le Monde en Francia; The Guardian en Gran Bretaña, la revista alemana Der Spiegel y otros medios establecen la conducta tras bambalinas de las relaciones internacionales de Washington, encubierta en público por lugares comunes, sonrisas y apretones de manos en sesiones fotográficas entre funcionarios de alto nivel.
La Casa Blanca condenó de inmediato la publicación de los documentos por parte de WikiLeaks, diciendo que "tales divulgaciones ponen en riesgo a nuestros diplomáticos, profesionales de inteligencia y a gente en todo el mundo que recurre a Estados Unidos en busca de asesoría en la promoción de la democracia y de un gobierno que rinda cuentas" (ver Pese al pedido de EE.UU., WikiLeaks dice que publicará los documentos secretos).
Los datos. ¿De dónde provienen los archivos de Wikileaks? Según se supo los datos se habrían filtrado desde SIPRnet (ver Wikipedia), una red de comunicaciones secreta del Pentágono."'''
print lin
l = tk.tokenize(lin);
ls = sp.split(l,0);
ls = mf.analyze(ls);
ls = tg.analyze(ls);
ls = sen.analyze(ls);
ls = nec.analyze(ls);
for s in ls :
    ws = s.get_words();
    for w in ws :
        print w.get_form()+" "+w.get_lemma()+" "+w.get_parole();
    
