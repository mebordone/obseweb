from django.db import models
import cPickle

# Create your models here.

class Articulo(models.Model) :
    medio = models.CharField(max_length=200)
    autor = models.CharField(max_length=200)
    seccion = models.CharField(max_length=200)
    titulo = models.CharField(max_length=200)
    volanta = models.CharField(max_length=200)
    bajada = models.CharField(max_length=200)
    texto = models.CharField(max_length=20000)
    fecha_bajada = models.DateTimeField('Fecha de Bajada')
    fecha_pub = models.DateTimeField('Fecha de Publicacion')
    #notas_rel = models.ManyToManyField(Articulo, verbose_name="Notas Relacionadas")
    num_comentarios = models.IntegerField()
    num_fotos = models.IntegerField()
    num_videos = models.IntegerField()
    titulo_tags = models.CharField(max_length=600)
    bajada_tags = models.CharField(max_length=600)
    texto_tags = models.CharField(max_length=60000)
    documentVector = models.CharField(max_length=100000)
    def __unicode__(self):
        uni = "%s %s" % (self.fecha_bajada,self.titulo)
        return uni

    def getDocumentVector(self):
        return cPickle.loads(self.documentVector)
    def setDocumentVector(self,vector):
        self.documentVector = cPickle.dumps(vector)
    
